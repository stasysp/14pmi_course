from dask.tests.test_base import np
from lxml import html
import urllib.request
from bs4 import BeautifulSoup, SoupStrainer
import pymystem3
import re
from collections import Counter
from math import *
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.metrics.pairwise import cosine_similarity

ind=0
flag=0
kolNextList=20
dictionary = []
fwd = open('Словарь новостей.txt', 'w')
kolFile=0
indFile=0
lemmatizedTexts=[]
Indices=[]
Indices1=[]
lemmJoinTexts=[]

topics={"culture", "health", "style", "education"}
for iTopic in range(len(topics)):
    nameCategory = topics.pop()
    print(nameCategory)
    nameLink="http://www.ng.ru/" + nameCategory + "/"
    htmlLink = urllib.request.urlopen(nameLink)
    soup = BeautifulSoup(htmlLink, 'html.parser', parse_only=SoupStrainer('a'))
    sameLink = 0
    links=[]

    nextLinkElement=0
    for idStr in range(kolNextList):
        if idStr>0:
            nextLink=nextLinkElement['href']
            htmlLink = urllib.request.urlopen("http://www.ng.ru" + nextLink)
            soup = BeautifulSoup(htmlLink, 'html.parser', parse_only=SoupStrainer('a'))
        for i in soup.find_all('a', href=True):
            if '.html' in i['href'] and i['href'].startswith('http') and 'www.ng' in i['href'] and nameCategory in i['href']:
                link=i['href']
                ind1=link.find('www.ng')
                ind2=link.find('.html')
                link=link[ind1:ind2+5]
                if sameLink==0:
                    link1=link
                    sameLink=1
                    links.append(link)
                else:
                    if not link==link1:
                        link1=link
                        links.append(link)
            if 'PAGEN_' in i['href']:
                nextLinkElement=i

    for item in links:
        # print(item)
        URL = "http://"+item
        page = html.parse(URL)
        title = page.getroot().find_class('htitle')[0].text
        # print(title)
        full_text = page.getroot().find_class('typical')[0]
        text = ""
        for el in full_text[1:]:
            if el.tag in ['p']:
                if not el.text==None:
                    text += el.text
                    for subel in el:
                        if subel.tag == 'a':
                            if not subel.text==None:
                                text += subel.text
                    text += '\n'
        text=title + " " + text       
        text=re.sub(r'[^\w\s]','', text)
        text=re.sub('\d','', text)
        text=re.sub(r'[\n\r\t]',' ', text)
        # print(text)

        mystem = pymystem3.Mystem()
        lemmatizedText = mystem.lemmatize(text)
        lemmatizedTexts.append(lemmatizedText)
        lemmText = np.array([x for x in lemmatizedText if not x.startswith(' ') and not x.startswith('\n')])
        lemmJoinText=""
        for item in lemmText:
            lemmJoinText+=item
        lemmJoinTexts.append(lemmJoinText)
        print(lemmatizedText)
        for i in range(len(lemmatizedText)):
            c=Counter(lemmatizedText)
            if not lemmatizedText[i].startswith(' '):
                if not lemmatizedText[i].startswith('\n'):
                    if ind > 0:
                        kol=0
                        for item in dictionary:
                            if item==lemmatizedText[i]:
                                flag=1
                                lastInIndices=Indices[dictionary.index(lemmatizedText[i])].pop()
                                if lastInIndices==indFile:
                                    Indices[dictionary.index(lemmatizedText[i])].append(indFile)
                                else:
                                    Indices[dictionary.index(lemmatizedText[i])].append(lastInIndices)
                                    Indices[dictionary.index(lemmatizedText[i])].append(indFile)
                            kol=kol+1
                    if flag==0:
                        dictionary.append(lemmatizedText[i])
                        Indices.append([])
                        Indices[dictionary.index(lemmatizedText[i])].append(indFile)

                        tf = c[lemmatizedText[i]] / len(text.split())
                        idf = log(kolNextList * 10 / len(Indices[dictionary.index(lemmatizedText[i])]))
                        tf_idf = tf * idf
                        Indices1.append(tf_idf)
                    ind=ind+1
                    flag=0
        indFile+=1

kol=0
for a1 in range(len(Indices)):
    strr = str(kol) + ": " + str(dictionary[a1]) + ": ("
    for a2 in range(len(Indices[a1])):
        strr+= str(Indices[a1][a2])
        if not a2==len(Indices[a1])-1:
            strr+=", "
        else:
            strr+="); "
    strr+="("+str(Indices1[a1])+"); "
    print(strr)
    fwd.write(strr+"\n")
    kol=kol+1
    
f = open('InputFile.txt', 'r')
text=f.read()
text=re.sub(r'[^\w\s]','', text)
text=re.sub('\d','', text)
text=re.sub(r'[\n\r\t]',' ', text)
mystem = pymystem3.Mystem()
lemmatizedText = mystem.lemmatize(text)
print(lemmatizedText)
ind=0
flag=0
uniqueInds=[]
strr1=""
for i in range(len(lemmatizedText)):
    if not lemmatizedText[i].startswith(' '):
        if not lemmatizedText[i].startswith('\n'):
            index0 = dictionary.index(lemmatizedText[i])
            for j in range(len(Indices[index0])):
                flag=0
                if len(uniqueInds)==0:
                    uniqueInds.append(Indices[index0][j])
                else:
                    for w in range(len(uniqueInds)):
                        if uniqueInds[w]==Indices[index0][j]:
                            flag=1
                            break
                    if flag==0:
                        uniqueInds.append(Indices[index0][j])
            strr1+=str(Indices1[index0])+" "
strr=""
for item in uniqueInds:
    strr+=str(item) + " "
f.close()
fw = open('OutputFile.txt', 'w')
print("1. Unique indices of Input file: " + strr)
print("2. tf_idf of Input file: " + strr1)
fw.write("1. Unique indices of Input file: " + strr + "\n")
fw.write("2. tf_idf of Input file: " + strr1 + "\n")

v = CountVectorizer()
tf = TfidfTransformer()
vect = v.fit_transform(lemmJoinTexts)
tfidf = tf.fit_transform(vect)

inputText = np.array([x for x in lemmatizedText if not x.startswith(' ') and not x.startswith('\n')])
inputText = np.unique(inputText)
inputJoinText=""
for item in inputText:
    inputJoinText+=item

request = np.unique(inputJoinText)
request_new = request
request_new = v.transform(request_new)
request_tfidf = tf.transform(request_new)

cosines = []
for i, vec in enumerate(tfidf):
    res = cosine_similarity(vec, request_tfidf)
    cosines.append([i, res[0][0]])
fw.write("3. Cosine: ")
for item in cosines:
    fw.write(str(item[1])+ " ")
print("3. Cosine:")
print(cosines)

