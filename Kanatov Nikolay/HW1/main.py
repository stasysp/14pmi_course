import re
import pandas as pd
from pymystem3 import Mystem

text = ""
paths=["./data/politics/text%d.txt","./data/cars/text%d.txt"]
m = Mystem()
wr = open('./output/textIndices.txt', 'w')
dictionary = {}
for path in paths:
    for i in range(1, 11):
        text=""
        f = open(path % i)
        lable = f.readline()
        text = f.read()
        f.close()
        text = re.sub(r'[^\w\s]',' ', text)
        text = re.sub('\d', '', text)
        text = re.sub(r'[\n\r\t]', ' ', text)
        lemmas = [a for a in m.lemmatize(text) if ' ' not in a]
        uniqueLemmas = list(set(lemmas))
        for lemma in uniqueLemmas:
            if dictionary.get(lemma) is None:
                dictionary.update({lemma:len(dictionary)+1})
        wr.write(lable)
        wr.write(' '.join([str(dictionary[a]) for a in lemmas]))
        wr.write('\n\n')
wr.close()

df = pd.DataFrame(list(dictionary.items()))
df.to_csv(path_or_buf='./output/dictionary.csv', sep=';',header=False, index=False)