import requests
import json
import datetime
import operator
from functools import reduce
import re

from lxml import html
from lxml.html import clean

from sklearn.feature_extraction.text import CountVectorizer
from pymystem3 import Mystem

BASE_URL = 'https://www.nn.ru/news/archive/{}/news/{}'
init_date = datetime.date(2017, 1, 1)
n_articles = 200

str_dates = [(init_date + datetime.timedelta(days=i)).strftime('%Y/%m/%d') for i in range(200)]

categories = ['pressrelease', 'society', 'incidents', 'world']

news_by_category = {category: [BASE_URL.format(category, date) for date in str_dates] for category in categories} 

def extract_links(url):
    response = requests.get(url)
    html_content = html.fromstring(response.content)
    links_query = html_content.xpath('//div/section/div/div/div/ul')[0]
    links = [link_obj[2] for link_obj in links_query.iterlinks()
             if 'news/more' in link_obj[2] and 'comments' not in link_obj[2]]
    return links

def extract_text(url):
    page = requests.get(url)
    html_content = html.fromstring(page.content)
    text_query = html_content.find_class('article-text')[0]
    text_query = clean.clean_html(text_query)
    return text_query.text_content()

def clean_text(text):
    text = text.strip()
    text = re.sub('\u200b', '', text)
    text = re.sub(r'\s+', ' ', text)
    return text

links_by_category = {k: list(set(reduce(operator.concat, map(extract_links, v))))[:n_articles]
                     for k, v in news_by_category.items()}

texts_by_category = {k: list(map(clean_text, map(extract_text, v)))
                     for k, v in links_by_category.items()}

for category, texts in texts_by_category.items():
    with open(category+'.txt', 'w') as f:
        f.write('\n'.join(texts))

ms = Mystem()
lemmatize = lambda x: ''.join(ms.lemmatize(x))

lemmatized_text_by_category = {k: ''.join(map(lemmatize, v))
                               for k, v in texts_by_category.items()}

vectorizer = CountVectorizer()
vectorizer.fit(lemmatized_text_by_category.values())

inverse_vocabulary = {str(v): k for k, v in vectorizer.vocabulary_.items()}

with open('dict.json', 'w') as f:
    f.write(json.dumps(inverse_vocabulary, ensure_ascii=False, indent=1))
