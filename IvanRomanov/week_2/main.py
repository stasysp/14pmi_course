from sklearn.datasets import fetch_20newsgroups
from functools import reduce
from itertools import combinations

newsgroups_train = fetch_20newsgroups(subset='train')

def search(query, iindex):
    n = len(query)
    c_to_index = {c: iindex.get(c) for c in query}
    c_to_index = {k: v for k, v in c_to_index.items() if v is not None}
    result = ([], [])
    for i in reversed(range(1, n+1)):
        for combination in combinations(c_to_index.keys(), i):
            response = reduce(set.intersection, map(set, [c_to_index[c] for c in combination]))
            if len(response) > len(result[1]):
                result = (combination, response)
        if len(result[1]) > 0:
            break
    return result

def query_iindex(query, k, newsgroups):
    words, result = search(query, iindex)
    result = list(result)
    
    print('Query, {0}, result: {0}'.format(query, words))
    
    if len(result) > 0:
        print('COUNT {0} results'.format(len(result)))
        for r in result[:k]:
            print(newsgroups_train.data[r])
    else:
        print('No result: {0}'.format(query))

def inverted_index(docs, words):
    iindex = {word: [] for word in words}
    
    for did, txt in docs.items():
        for word in txt:
            iindex[word] += [did]
    
    return iindex

def read_newsgroups(newsgroups_data):
    docs, words = {}, set()
    
    for idx, item in enumerate(newsgroups_data):
        txt = item.split()
        txt = [t.lower() for t in txt]
        words |= set(txt)
        docs[idx] = txt
    return docs, words

docs, words = read_newsgroups(newsgroups_train.data)
iindex = inverted_index(docs, words)
query_iindex(['shortstop', 'who', 'had', 'gvfhtr'], 3, newsgroups_train)

