import os
import numpy as np
from pymystem3 import Mystem
import re
import pandas as pd

folder = ".\\dataset"

FileList = []
for dirname in os.listdir(folder):
     path = os.path.join(folder, dirname)
     for filename in os.listdir(path):
          FileList.append(os.path.join(folder, dirname, filename))

m = Mystem()
textAll = ""
dictLemmas = ({})
for file in FileList:
    f = open(file)
    text = f.read()
    textAll += text
    tokens = re.findall('\w+', text)
    text = ' '.join(tokens)
    tokens = re.findall(r'\b[^1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ]\w+', text)
    text = ' '.join(tokens)
    lemmas = m.lemmatize(text)
    words = np.array([x for x in lemmas if x != '\n' and x != ' ' and x[0] != 'Х'])
    dictLemmas[file] = words

tokens = re.findall('\w+', textAll)
text = ' '.join(tokens)
tokens = re.findall(r'\b[^1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ]\w+', text)
text = ' '.join(tokens)
lemmas = m.lemmatize(text)
words = np.unique(lemmas)
words = [x for x in words if x !='\n' and x !=' ' and x[0] !='Х']
words.sort()

dictionary = pd.DataFrame(data=words, columns=["Words"])
dictionary.to_csv(path_or_buf='dict.csv', sep=';')

dict = {a: (list(words).index(a)) for a in words}
f = open('indexes.txt', 'w')
for k in dictLemmas.keys():
    f.write("\n\n" + k + "\n")
    for token in dictLemmas[k]:
        f.write(re.sub(token, str(dict.get(token)), token) + " ")