
# coding: utf-8

# ## Engine:

# In[79]:

textEngine = str()
for i in range(1, 11):
    f = open('C:/Nikita/учёба/Git/14pmi_course/KorobkovNikita/HW1/data/engine/engine%d.txt' % i)
    textEngine = textEngine + f.read()


# In[80]:

import re
textEngineClean = re.sub(r'[^\w\s]','', textEngine)
textEngineClean = re.sub('\d', '', textEngineClean)
textEngineClean = re.sub(r'[\n\r\t]', ' ', textEngineClean)


# In[81]:

from pymystem3 import Mystem


# In[4]:

m = Mystem()


# In[82]:

lemmasEngine = m.lemmatize(textEngineClean)


# In[83]:

while True:
    try:
        lemmasEngine.remove(' ')
    except:
        break
while True:
    try:
        lemmasEngine.remove('  ')
    except:
        break
while True:
    try:
        lemmasEngine.remove('   ')
    except:
        break
while True:
    try:
        lemmasEngine.remove('    ')
    except:
        break
while True:
    try:
        lemmasEngine.remove('\n')
    except:
        break


# In[84]:

uniqueLemmasEngine = list(set(lemmasEngine))


# In[85]:

dictEngine = {a: (uniqueLemmasEngine.index(a) + 1) for a in uniqueLemmasEngine}


# In[113]:

textListEngine = list()
for token in lemmasEngine:
    textListEngine.append(re.sub(token, str(dictEngine.get(token)), token))


# In[114]:

wr = open('textIndEngine.txt', 'w')
wr.write(' '.join(str(a) for a in textListEngine))
wr.close()


# In[136]:

wr = open('dictIndEngine.txt', 'w')
wr.write(''.join(str(a) + ' ' + str(dictEngine.get(a)) + '\n' for a in dictEngine.keys()))
#wr.write(''.join(str(a)+'\n' for a in dictEngine.items()))
wr.close()


# ## Space:

# In[115]:

textSpace = str()
for i in range(1, 11):
    f = open('C:/Nikita/учёба/Git/14pmi_course/KorobkovNikita/HW1/data/space/space%d.txt' % i)
    textSpace = textSpace + f.read()


# In[116]:

import re
textSpaceClean = re.sub(r'[^\w\s]','', textSpace)
textSpaceClean = re.sub('\d', '', textSpaceClean)
textSpaceClean = re.sub(r'[\n\r\t]', ' ', textSpaceClean)


# In[117]:

lemmasSpace = m.lemmatize(textSpaceClean)


# In[118]:

while True:
    try:
        lemmasEngine.remove(' ')
    except:
        break
while True:
    try:
        lemmasEngine.remove('  ')
    except:
        break
while True:
    try:
        lemmasEngine.remove('   ')
    except:
        break
while True:
    try:
        lemmasEngine.remove('    ')
    except:
        break
while True:
    try:
        lemmasEngine.remove('\n')
    except:
        break


# In[119]:

uniqueLemmasSpace = list(set(lemmasSpace))
dictSpace = {a: (uniqueLemmasSpace.index(a) + 1) for a in uniqueLemmasSpace}


# In[120]:

textListSpace = list()
for token in lemmasSpace:
    textListSpace.append(re.sub(token, str(dictSpace.get(token)), token))


# In[121]:

wr = open('textIndSpace.txt', 'w')
wr.write(' '.join(str(a) for a in textListSpace))
wr.close()


# In[137]:

wr = open('dictIndSpace.txt', 'w')
wr.write(''.join(str(a) + ' ' + str(dictSpace.get(a)) + '\n' for a in dictSpace.keys()))
wr.close()

