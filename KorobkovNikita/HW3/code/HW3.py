
# coding: utf-8

# In[1]:

from lxml import objectify, etree, html
import urllib
from pymystem3 import Mystem
import xml.etree.ElementTree as ET
import sys


# In[40]:

import pandas as pd
import numpy as np
import re


# In[2]:

main_url = 'https://ria.ru'


# In[3]:

topics = ['religion', 'economy', 'politics', 'science']


# Собираем ссылки на новости по датам

# In[5]:

def getLinks(topic):
    links = []
    rng = pd.date_range('1/9/2018', periods=30, freq='D')
    for d in rng:
        date = '' + str(d.year)
        if len(str(d.month)) == 1:
            date += '0'
        date += str(d.month)
        if len(str(d.day)) == 1:
            date += '0'
        date += str(d.day)
        try:    
            URL = main_url + '/' + topic + '/' + date + '/'
            tree = html.parse(urllib.request.urlopen(URL))
            articles = tree.getroot().find_class('b-list__item')
            for article in articles:
                l = article.getchildren()[0].attrib['href']
                if l[0:8] != "https://":
                    links.append(main_url + l)     
        except:
            pass
    return links


# Собираем ссылки на статьи по каждой дате

# In[34]:

links = ({})
for topic in topics:
    links[topic] = getLinks(topic)


# In[35]:

links


# In[37]:

for topic in topics:
    print(topic + " =", len(links[topic]))


# In[9]:

def getText(link):
    try:
        tree = html.parse(urllib.request.urlopen(link))
        title = tree.getroot().find_class('b-article__title')[0][0].text
        article = tree.getroot().find_class('b-article__body js-mediator-article mia-analytics')
        text = ""
        for t in article[0]:
            if len(t) == 0:
                if t.tail is not None:
                    text += " " + t.tail
                if t.text is not None:
                    text += " " + t.text
            else:
                for i in t:
                    if i.tail is not None:
                        text += " " + i.tail
    except:
        return None, None
    return title, text


# Собираем тексты из ссылок.

# In[10]:

allTexts = ({})
for topic in topics:
    texts = []
    i = 0
    for link in links[topic]:
        title, text = getText(link)
        if(title != None and text != None):
            i+=1
            texts.append((title, text))
            sys.stdout.write(topic + ": " + str(i+1) + "/" + str(N) + "\r")
        if(i >= 200):
            break
    allTexts[topic] = texts
    print(topic + " 100/100%")


# In[39]:

for topic in topics:
    print(topic + " =", len(allTexts[topic]))


# In[12]:

allTexts


# In[13]:

m = Mystem()
dictLemmas = ({})
alltokens = []


# Чистим тексты и делаем лематизацию для каждой группы отдельно. Отдельно потому что памяти на всё не хватает и выходит ошибка.

# In[14]:

topic = "religion"
tokensTopic = []
i = 0
for pair in allTexts[topic]:
    title = pair[0]
    text = pair[1]
    texts = text.split('.')
    sentances = []
    for text in texts:
        tokens = re.findall('\w+', text)
        text = ' '.join(tokens)
        tokens = re.findall(r'\b[^1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ]\w+', text)
        text = ' '.join(tokens)
        lemmas = m.lemmatize(text)
        words = np.array([x for x in lemmas if x != '\n' and x != ' ']) 
        for w in words:
            alltokens.append(w)
        sentances.append(words)
    if(len(sentances[-1]) == 0):
        sentances.pop(-1)
    tokensTopic.append((title, sentances))
    sys.stdout.write(topic + ": " + str(i+1) + "/" + str(len(allTexts[topic]))+"\r")
    i+=1
dictLemmas[topic] = tokensTopic
print(topic + " 100/100%")


# In[15]:

topic = "economy"
tokensTopic = []
i = 0
m = Mystem()
for pair in allTexts[topic]:
    title = pair[0]
    text = pair[1]
    texts = text.split('.')
    sentances = []
    for text in texts:
        tokens = re.findall('\w+', text)
        text = ' '.join(tokens)
        tokens = re.findall(r'\b[^1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ]\w+', text)
        text = ' '.join(tokens)
        lemmas = m.lemmatize(text)
        words = np.array([x for x in lemmas if x != '\n' and x != ' ']) 
        for w in words:
            alltokens.append(w)
        sentances.append(words)
    if(len(sentances[-1]) == 0):
        sentances.pop(-1)
    tokensTopic.append((title, sentances))
    sys.stdout.write(topic + ": " + str(i+1) + "/" + str(len(allTexts[topic]))+"\r")
    i+=1
dictLemmas[topic] = tokensTopic
print(topic + " 100/100%")


# In[16]:

topic = "politics"
tokensTopic = []
i = 0
m = Mystem()
for pair in allTexts[topic]:
    title = pair[0]
    text = pair[1]
    texts = text.split('.')
    sentances = []
    for text in texts:
        tokens = re.findall('\w+', text)
        text = ' '.join(tokens)
        tokens = re.findall(r'\b[^1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ]\w+', text)
        text = ' '.join(tokens)
        lemmas = m.lemmatize(text)
        words = np.array([x for x in lemmas if x != '\n' and x != ' ']) 
        for w in words:
            alltokens.append(w)
        sentances.append(words)
    if(len(sentances[-1]) == 0):
        sentances.pop(-1)
    tokensTopic.append((title, sentances))
    sys.stdout.write(topic + ": " + str(i+1) + "/" + str(len(allTexts[topic]))+"\r")
    i+=1
dictLemmas[topic] = tokensTopic
print(topic + " 100/100%")


# In[17]:

topic = "science"
tokensTopic = []
i = 0
m = Mystem()
for pair in allTexts[topic]:
    title = pair[0]
    text = pair[1]
    texts = text.split('.')
    sentances = []
    for text in texts:
        tokens = re.findall('\w+', text)
        text = ' '.join(tokens)
        tokens = re.findall(r'\b[^1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ]\w+', text)
        text = ' '.join(tokens)
        lemmas = m.lemmatize(text)
        words = np.array([x for x in lemmas if x != '\n' and x != ' ']) 
        for w in words:
            alltokens.append(w)
        sentances.append(words)
    if(len(sentances[-1]) == 0):
        sentances.pop(-1)
    tokensTopic.append((title, sentances))
    sys.stdout.write(topic + ": " + str(i+1) + "/" + str(len(allTexts[topic]))+"\r")
    i+=1
dictLemmas[topic] = tokensTopic
print(topic + " 100/100%")


# In[18]:

dictLemmas


# Делаем словарь с индексами

# In[33]:

words = np.unique(alltokens)
words.sort()
dictionary = pd.DataFrame()
dictionary["Words"] = words


# In[20]:

dictionary.to_csv(path_or_buf='dictionary.csv', sep=';')


# In[21]:

dictWordToIndex = {w: (list(words).index(w)) for w in words}
dictWordToIndex


# Переделываем тексты из слов в индыксы

# In[22]:

dictLemmasIndex = ({})
for topic in topics:
    indexTopic = []
    for pair in dictLemmas[topic]:
        title = pair[0]
        sentances = pair[1]
        indexText = []
        for lemmas in sentances:
            indexSentances = []
            for lemma in lemmas:
                try:
                    indexSentances.append(dictWordToIndex[lemma])
            indexText.append(indexSentances)
        indexTopic.append((title, indexText))
    dictLemmasIndex[topic] = indexTopic


# In[23]:

dictLemmasIndex


# In[24]:

for word in dictLemmas["religion"][0][1][0][0:3]:
    print(word, "-", dictWordToIndex[word])


# Делаем .xml

# In[26]:

root = etree.Element('Head')
for topic in topics:
    childTopic = etree.Element("Topic", dict(TopicName = topic))
    for i in range(0, len(dictLemmasIndex[topic])):
        childText = etree.Element("Text", dict(Id = str(i), Title=dictLemmasIndex[topic][i][0]))
        for j in range(0, len(dictLemmasIndex[topic][i][1])):
            childSen = etree.Element("Sentances")
            string = ""
            for s in dictLemmasIndex[topic][i][1][j]:
                string += str(s) + " "
            childSen.text = string
            childText.append(childSen)
        childTopic.append(childText)
    root.append(childTopic)
mytree = etree.ElementTree(root)
mytree.write("file.xml", pretty_print=True, xml_declaration=True, encoding="utf-8")


# In[30]:

np.save('allTexts.npy', allTexts)


# In[31]:

read_dictionary = np.load('allTexts.npy').item()


# In[32]:

read_dictionary

