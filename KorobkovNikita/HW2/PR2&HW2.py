
# coding: utf-8

# In[1]:

import os
import numpy as np
from pymystem3 import Mystem
import re
import pandas as pd
import math


# In[2]:

folder = "C://Nikita//учёба//Git//14pmi_course//KorobkovNikita//HW1//data//"

FileList = []
for dirname in os.listdir(folder):
    path = os.path.join(folder, dirname)
    for filename in os.listdir(path):
        FileList.append(os.path.join(folder, dirname, filename))


# In[3]:

m = Mystem()
textAll = ""
dictLemmas = ({})
for file in FileList:
    f = open(file)
    text = f.read()
    textAll += text
    tokens = re.findall('\w+', text)
    text = ' '.join(tokens)
    tokens = re.findall(r'\b[^1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ]\w+', text)
    text = ' '.join(tokens)
    lemmas = m.lemmatize(text)
    words = np.array([x for x in lemmas if x != '\n' and x != ' ' and x[0] != 'Х'])
    dictLemmas[file] = words
    f.close()


# In[4]:

tokens = re.findall('\w+', textAll)
text = ' '.join(tokens)
tokens = re.findall(r'\b[^1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ]\w+', text)
text = ' '.join(tokens)
lemmas = m.lemmatize(text)
words = np.unique(lemmas)
words = [x for x in words if x !='\n' and x !=' ' and x[0] !='Х']
words.sort()


# In[5]:

dictionary = pd.DataFrame(data=words, columns=["Words"])
dictionary.to_csv(path_or_buf='dict.csv', sep=';')

dict = {a: (list(words).index(a)) for a in words}
f = open('indexes.txt', 'w')
for k in dictLemmas.keys():
    f.write("\n\n" + k + "\n")
    for token in dictLemmas[k]:
        f.write(re.sub(token, str(dict.get(token)), token) + " ")


# In[6]:

dictt = {a: (list(words).index(a)) for a in words}


# In[7]:

R = 15
N = 30


# In[8]:

a = []
for word in dictt.keys():
    ri = 0
    for file in FileList[0:15]:
        if(list(dictLemmas[file]).count(word)>0):
            ri+=1
    a.append(ri/R)


# In[9]:

b = []
for word in dictt.keys():
    ri = 0
    for file in FileList[15:30]:
        if(list(dictLemmas[file]).count(word)>0):
            ri+=1
    b.append(ri/(N-R))


# In[10]:

c = []
Cc = 0
for i in range(0, len(a)):
    ci = (a[i]*(1-b[i])+0.5)/(b[i]*(1-a[i])+0.5)
    c.append(math.log(ci))
    Cc += math.log((1-a[i]+0.1)/(1-b[i]+0.1))


# In[11]:

Cc


# In[12]:

parametrs = pd.DataFrame()
parametrs["words"] = dictt.keys()
parametrs["a"] = a
parametrs["b"] = b
parametrs["c"] = c


# In[13]:

parametrs.sort_values("c")


# In[14]:

parametrs.sort_values("c").to_csv(path_or_buf='parametrs.csv', sep=';')


# Доделать g и сделать по релевантность по запросу.

# In[22]:

request = input()
#request = request.split(" ")
request = m.lemmatize(request)
d = 0
for r in request:
    if(r!=' ' and r!='\n' and dictt.get(r) != None):
        d += parametrs.iloc[dictt.get(r)]["c"]
d

